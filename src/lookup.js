const { MessageEmbed } = require('discord.js');
const axios = require('axios');
const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('lookup')
        .setDescription('Lookup users or posts or posts by the user via there ID or Username from anime.pics')
        .addStringOption(option => option.setName('user').setDescription('Look up a user with their username'))
        .addStringOption(option => option.setName('post').setDescription('Spesific post via its ID'))
        .addStringOption(option => option.setName('posts').setDescription('All posts from a User with their username')),
    async execute(interaction) {
        const postId = interaction.options.getString('post')
        const userName = interaction.options.getString('user')
        const postsFromUser = interaction.options.getString('posts')
        if ( postId ) {
            const rApi = await axios.get(`https://gateway.anime.pics/post/${postId}`)
            const embed = new MessageEmbed()
                .setTitle(`${rApi.data.title} - ${rApi.data.id}`)
                .setImage(`https://content.anime.pics/${rApi.data.id}`)
                .setDescription(`${rApi.data.description} \n\n ❤️: ${rApi.data.likes} | 👀: ${rApi.data.views}`)
                .setFooter(`Posted by ${rApi.data.username} on ${rApi.data.posted}`)
            return interaction.reply({ embeds: [embed]})
        }
        else if ( postsFromUser ) {
            const rApiUserData = await axios.get(`https://gateway.anime.pics/user/${postsFromUser}`)
            const rApi = await axios.get(`https://gateway.anime.pics/posts/${rApiUserData.data.id}`)
            const embed = new MessageEmbed()
            .setTitle(`Posts from ${rApiUserData.data.username} - ${rApiUserData.data.id}`)
            for (const field of rApi.data) {
                embed.addFields({ name: `${field.title} - ${field.id}`, value: `❤️ Likes: ${field.likes} | 👀 Views: ${field.views} | [Link](https://anime.pics/p/${field.id}) \n ${field.description}` })
            }
            embed.setFooter(`Total posts: ${rApi.data.length}`)
            return interaction.reply({ embeds: [embed]})
        }
        else if ( userName ) {
            const rApi = await axios.get(`https://gateway.anime.pics/user/${userName}`)
            let twitter = rApi.data.twitter
            if (!twitter) twitter = ' Not provided'
            const embed = new MessageEmbed()
                .setTitle(`${rApi.data.username} - ${rApi.data.id}`)
                .setImage(`https://content.anime.pics/${rApi.data.avatar}`)
                .setDescription(`${rApi.data.description} \n\n 👀 Total followers: ${rApi.data.follower} \n 📃 Total posts: ${rApi.data.post_count} \n 🐦 Twitter: ${twitter}`)
            return interaction.reply({ embeds: [embed]})
        }
        else {
            return interaction.reply({ content: 'Nyaa... i have nothing to lookup for. Do /lookup user: or /lookup posts:', ephemeral: true });
        }
    },
};