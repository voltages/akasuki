const { MessageEmbed } = require('discord.js');
const axios = require('axios');
const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('feed')
        .setDescription('Pull the posts feed from anime.pics')
        .addNumberOption(option => option.setName('num').setDescription('Enter a number to offset').setRequired(true)),
    async execute(interaction) {
        const offset = interaction.options.getNumber('num')
        const rApi = await axios.get(`https://gateway.anime.pics/feed/${offset}`)
            const embed = new MessageEmbed()
            .setTitle(`${rApi.data[0].title}`)
            .setImage(`https://content.anime.pics/${rApi.data[0].id}`)
            .setDescription(`${rApi.data[0].description} \n\n ❤️: ${rApi.data[0].likes} | 👀: ${rApi.data[0].views}`)
            .setFooter(`Posted by ${rApi.data[0].username}`)
        if (offset) return interaction.reply({ embeds: [embed]});
        return interaction.reply({ embeds: [embed]});
    },
};
