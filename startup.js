// Filesystem module
const fs = require('fs');
// Requires the Discord.js to be downloaded.
const { Client, Intents, Collection } = require('discord.js');
// Requires dotenv to be installed and to be able to use the clients required discord token
require("dotenv/config");

const client = new Client({ intents: [Intents.FLAGS.GUILDS] });
client.commands = new Collection();
const commandFiles = fs.readdirSync('./src').filter(file => file.endsWith('.js'));

for (const file of commandFiles) {
	const command = require(`./src/${file}`);
	client.commands.set(command.data.name, command);
}

client.once('ready', () => {
	console.clear();
	console.log(`[INIT] Operational, currently in ${client.guilds.cache.size} guild(s)`);
});

client.on('interactionCreate', async interaction => {
	if (!interaction.isCommand()) return;

	const command = client.commands.get(interaction.commandName);

	if (!command) return;

	try {
		await command.execute(interaction);
	} catch (error) {
		console.error(error);
		return interaction.reply({ content: `There was an error during the execution of this command... \n<${error}>`, ephemeral: true });
	}
});

client.login(process.env.DISCORD_TOKEN);